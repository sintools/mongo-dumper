import os
from itertools import zip_longest
from logging import getLevelName
from pathlib import Path
from typing import Any, Iterable, Iterator, cast

from logzero import logger
from logzero import loglevel as set_logzero_level
from pymongo import MongoClient
from tqdm.auto import tqdm


def set_loglevel(level: str = "debug") -> None:
    level = level.upper()
    logger.info("Setting logging level to %s", level)

    set_logzero_level(getLevelName(level))


class BasicMenu:
    def __init__(
        self,
        path: str,
        host: str | None = None,
        port: int | None = None,
        db: str | None = None,
        loglevel: str = "info",
    ) -> None:
        logger.debug("loglevel: %s", loglevel)
        self.path = Path(path)
        set_loglevel(loglevel)

        host = cast(str, host or os.environ.get("MONGO_HOST", "localhost"))
        port = int(os.environ.get("MONGO_PORT", 27017)) if port is None else port

        self.dbname = cast(str, db or os.environ.get("MONGO_DB", "imported"))
        self.client: MongoClient = MongoClient(host, port)

    def _assert_path(self) -> None:
        if not self.path.exists():
            raise FileNotFoundError(f"Path {self.path} does not exist")

    def __del__(self) -> None:
        logger.debug("Closing mongo client")
        self.client.close()


def wc(path: Path) -> int:
    line_count: int = sum(
        1 for _ in tqdm(path.open("r"), "Counting lines", leave=False)
    )
    logger.debug('Read %d lines in "%s"', line_count, path)
    return line_count


class Batcher:
    def __init__(self, iterable: Iterable[Any], n: int, fillvalue: Any = None) -> None:
        self.iterable: Iterator[Any] = iter(iterable)
        self.n: int = n
        self.fillvalue = fillvalue

    def __iter__(self) -> Iterator[Iterator[Any]]:
        args = [self.iterable] * self.n
        return zip_longest(*args, fillvalue=self.fillvalue)

    def count(self, no_items: int) -> int:
        return (no_items + self.n - 1) // self.n
