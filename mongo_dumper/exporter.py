import csv
import json
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Callable, Collection, Type

from fire import Fire
from logzero import logger
from tqdm import tqdm

from mongo_dumper.common import BasicMenu

EXPORTERS: dict[Any, Any] = {}


class ExporterBase(ABC):
    def __init__(self, outpath: Path) -> None:
        logger.debug('Creating exporter to "%s"', outpath)
        self.outpath = Path(outpath)
        self.outpath.parent.mkdir(exist_ok=True, parents=True)

    @abstractmethod
    def add(self, item: dict[str, Any]) -> None:
        raise NotImplementedError


def register_exporter(*extensions: str) -> Callable[[Type[ExporterBase]], Type[ExporterBase]]:
    def wrapper(cls: Type[ExporterBase]) -> Type[ExporterBase]:
        for ext in extensions:
            EXPORTERS[f".{ext}"] = cls
        return cls

    return wrapper


@register_exporter("csv")
class CsvExporter(ExporterBase):
    def __init__(self, outpath: Path) -> None:
        super().__init__(outpath)
        self.fhd = self.outpath.open("w")
        self.csv_writer: csv.DictWriter | None = None

    def add(self, item: dict[str, Any]) -> None:
        if self.csv_writer is None:
            logger.debug('%s - opening writer to "%s"', self.__class__.__name__, self.outpath)
            self.csv_writer = csv.DictWriter(self.fhd, item.keys())

            logger.debug("%s - writing header: %s", self.__class__.__name__, ", ".join(item.keys()))
            self.csv_writer.writeheader()

        logger.debug("%s - writing row: %s", self.__class__.__name__, item)
        self.csv_writer.writerow(item)

    def __del__(self) -> None:
        logger.debug("%s - closing file handler", self.__class__.__name__)
        self.fhd.close()


@register_exporter("jl", "jsonl")
class JsonLinesExporter(ExporterBase):
    def __init__(self, outpath: Path) -> None:
        super().__init__(outpath)
        logger.debug('%s - opening writer to "%s"', self.__class__.__name__, self.outpath)
        self.fhd = open(self.outpath, "w")

    def add(self, item: dict[str, Any]) -> None:
        logger.debug("%s - writing row: %s", self.__class__.__name__, item)
        json.dump(item, self.fhd, default=str)
        self.fhd.write("\n")

    def __del__(self) -> None:
        logger.debug("%s - closing file handler", self.__class__.__name__)
        self.fhd.close()


class Exporter(BasicMenu):
    def __init__(
        self,
        outroot: str,
        db: str,
        formats: tuple[str, ...] | str = ("csv", "jl"),
        cols: str | tuple[str, ...] | None = None,
        loglevel: str = "info",
        host: str | None = None,
        port: int | None = None,
    ) -> None:
        super().__init__(outroot, host, port, db, loglevel)
        self._assert_path()

        match formats:
            case str():
                self.formats = [formats]
            case tuple() | list():
                self.formats = list(formats)

        self.db = self.client[self.dbname]

        self.cols: Collection[str]
        match cols:
            case str():
                self.cols = [cols]
            case tuple():
                self.cols = cols
            case None:
                self.cols = self.db.list_collection_names()
            case _:
                raise NotImplementedError(f"cols={cols} is not supported")

    def _assert_path(self) -> None:
        if not self.path.exists():
            logger.warning('Path "%s" does not exist. Creating...', self.path)
            self.path.mkdir(parents=True)

    def _get_exporters(self, col_name: str) -> Collection[ExporterBase]:
        exporters: list[ExporterBase] = []

        for outformat in self.formats:
            outpath = (self.path / outformat / col_name).with_suffix(f".{outformat}")

            if exporter := EXPORTERS.get(f".{outformat}", None):
                exporters.append(exporter(outpath))

        if not exporters:
            raise ValueError(f"Could not find any exporter for {self.formats}")

        return exporters

    def _dump(self) -> None:
        for col_name in tqdm(
            self.cols,
            f"{self.dbname} collections",
            unit="collection",
            leave=False,
            dynamic_ncols=True,
            disable=len(self.cols) == 1,
        ):
            col = self.db[col_name]
            exporters = self._get_exporters(col_name)

            for item in tqdm(
                col.find(projection={"_id": False}),
                f"Dumping {col.name}",
                col.count_documents({}),
                unit="row",
                leave=False,
                dynamic_ncols=True,
            ):
                for exporter in exporters:
                    exporter.add(item)

    __call__ = _dump


def fire_exporter() -> None:
    Fire(Exporter)


if __name__ == "__main__":
    fire_exporter()
