from typing import Type

from mongo_dumper.exporter import Exporter
from mongo_dumper.importer import Importer


class Cli:
    def importer(self) -> Type[Importer]:
        return Importer

    def exporter(self) -> Type[Exporter]:
        return Exporter
