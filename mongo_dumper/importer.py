import json
from collections.abc import Iterable
from functools import lru_cache
from itertools import chain
from pathlib import Path
from typing import Any, Callable, Generator, Sized, Type, cast

from fire import Fire
from logzero import logger
from tqdm import tqdm

from mongo_dumper.common import BasicMenu, Batcher, wc


class DumpLoader(Sized):
    path: Path
    file_opener: Callable[..., Any] = open
    file_mode: str = "r"

    def __init__(self, path: Path) -> None:
        self.path = path

        if not self.path.exists():
            raise FileNotFoundError(f"Path {path} does not exist")

        if not self.path.is_file():
            raise FileNotFoundError(f"Path {path} is not a file")

        self.fhd = self.file_opener(self.path, self.file_mode)

    def __del__(self) -> None:
        if not self.fhd.closed:
            self.fhd.close()

    def __iter__(self) -> Generator[dict[str, Any] | list[Any], None, None]:
        raise NotImplementedError

    def __len__(self) -> int:
        raise NotImplementedError


def register_loader(*extensions: str) -> Callable[[Type[DumpLoader]], Type[DumpLoader]]:
    def wrapper(cls: Type[DumpLoader]) -> Type[DumpLoader]:
        for ext in extensions:
            LOADERS[f".{ext}"] = cls
        return cls

    return wrapper


LOADERS: dict[str, Type[DumpLoader]] = {}


@register_loader("jsonl", "jl")
class JlDumpLoader(DumpLoader):
    def __iter__(self) -> Generator[dict[str, Any] | list[Any], None, None]:
        for line in self.fhd:
            yield json.loads(line)

    @lru_cache
    def __len__(self) -> int:
        return wc(self.path)


class Importer(BasicMenu):
    def __init__(
        self,
        path: str,
        loglevel: str = "info",
        host: str | None = None,
        port: int | None = None,
        db: str | None = None,
        batchsize: int = 50,
        limit: int | None = None,
    ) -> None:
        super().__init__(path, host, port, db, loglevel)
        self._assert_path()
        self.db = self.client[self.dbname]
        self.batchsize = batchsize
        self.limit = limit

    def _find_filepaths(self) -> list[Path]:
        if self.path.is_file():
            files = [self.path]
        elif self.path.is_dir():
            files = list(chain.from_iterable(self.path.glob(f"*{ext}") for ext in LOADERS))
            logger.info("Found %d files to import", len(files))
        else:
            raise FileNotFoundError(f"Path {self.path} is not a file or directory")

        return files

    def _insert_from_importer(self, importer: DumpLoader, colname: str) -> None:
        col = self.db[colname]
        batches = Batcher(cast(Iterable, importer), self.batchsize)
        inserted = 0

        for batch in tqdm(
            batches,
            f"Importing {importer.path}",
            batches.count(len(importer)),
            unit="item",
            leave=False,
            dynamic_ncols=True,
        ):
            if self.limit:
                if inserted >= self.limit:
                    logger.info("Limit reached, stopping")
                    break

                if inserted + len(batch) > self.limit:
                    batch = batch[: self.limit - inserted]

            result = col.insert_many(list(filter(None, batch)), ordered=False)

            if result.acknowledged:
                logger.debug("Inserted %d items", len(result.inserted_ids))
                inserted += len(result.inserted_ids)
            else:
                logger.error("Insertion failed")

        logger.info("Inserted %d items to %s", inserted, col.name)

    def __call__(self) -> None:
        paths = self._find_filepaths()

        for path in tqdm(paths, "Importing files", unit="file", leave=False, disable=len(paths) == 1):
            if (loader_cls := LOADERS.get(path.suffix)) is None:
                logger.error('No loader found for file "%s"', path)
                continue

            loader = loader_cls(path)

            if not isinstance(loader, Sized):
                raise NotImplementedError("Not iterable importer is not implemented yet")

            self._insert_from_importer(loader, path.stem)


def fire_importer() -> None:
    Fire(Importer)


if __name__ == "__main__":
    fire_importer()
