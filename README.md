# Mongo data exporter/importer

## Installation

```bash
poetry install
```





### mongo-import
```
SYNOPSIS
    mongo-import --path=PATH <flags>

ARGUMENTS
    PATH
        Type: str

FLAGS
    -h, --host=HOST
        Type: Optional[str | None]
        Default: None
    -p, --port=PORT
        Type: Optional[int | None]
        Default: None
    -d, --db=DB
        Type: Optional[str | None]
        Default: None
    -b, --batchsize=BATCHSIZE
        Type: int
        Default: 50
    -l, --loglevel=LOGLEVEL
        Type: str
        Default: 'info'
```
### mongo-export
```

SYNOPSIS
    mongo-export --outroot=OUTROOT --db=DB <flags>

ARGUMENTS
    OUTROOT
        Type: str
    DB
        Type: str

FLAGS
    -f, --formats=FORMATS
        Type: tuple[str, ...] | str
        Default: ('csv', 'jl')
    -c, --cols=COLS
        Type: Optional[str | tuple[str, ...]]
        Default: None
    -h, --host=HOST
        Type: Optional[str | None]
        Default: None
    -p, --port=PORT
        Type: Optional[int | None]
        Default: None
    -l, --loglevel=LOGLEVEL
        Type: str
        Default: 'info'
```




